<?xml version="1.0" encoding="UTF-8"?>
<ScrInfo ScreenNo="6" ScreenType="" ScreenSize="0">
	<Script>
		<TrigAction>
			<Trigger Action="1" BitAddr="HDX10.2">@W_HDW8010 = 1
@W_HDW8011 = 1020
@W_HDW8012 = 90
@W_HDW8013 = 25
@W_HDW8014 = 10
'@W_HDW8015 = 1
@W_HDW8016 = 0
@W_HDW8017 = 2000
</Trigger>
		</TrigAction>
		<TimerAction>
		</TimerAction></Script>
<PartInfo PartType="Text" PartName="TXT_5">
<General TextContent="Kd, 0...32767" LaFrnColor="0x0 0" IsBackColor="0" BgColor="0xffffff 0" CharSize="176 126 126 126 126 126 126 12" Bold="0" StartPt="66 165"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
<PartInfo PartType="Rect" PartName="REC_0">
<General Area="13 7 292 36" BorderColor="0x0 0" Pattern="1" FrnColor="0x9bffff -1" BgColor="0x80ff -1" ActiveColor="0"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
<PartInfo PartType="Text" PartName="TXT_0">
<General TextContent="PID -- НЕПРЕРЫВНЫЙ РЕЖИМ" LaFrnColor="0x0 0" IsBackColor="0" BgColor="0xffffff 0" CharSize="166 126 126 126 126 126 126 12" Bold="0" StartPt="22 13"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
<PartInfo PartType="Text" PartName="TXT_1">
<General TextContent="Ts, 0.001...32.767 с" LaFrnColor="0x0 0" IsBackColor="0" BgColor="0xffffff 0" CharSize="176 126 126 126 126 126 126 12" Bold="0" StartPt="64 53"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
<PartInfo PartType="Numeric" PartName="NUM_0">
<General Desc="NUM_0" Area="210 48 297 72" WordAddr="HDW8010" Fast="0" IsInput="1" WriteAddr="HDW8010" KbdScreen="1000" IsPopKeyBrod="0" FigureFile="TFT-type style\dp_zc00.pvg" BorderColor="0xcccccc 0" FrnColor="0x0 0" BgColor="0xffffff 0" BmpIndex="-1" Transparent="0" IsHideNum="0" HighZeroPad="0" IsShowPwd="0" UseGlint="0" GlintFgClr="0x0 0" ZeroNoDisplay="0" IsIndirectR="0" IsIndirectW="0" IsAddFrame="0" IsWordOrder="0"/>
<DispFormat DispType="2" DigitCount="2 3" DataLimit="981668463 1107497320" DataRange="0.001000 32.767000" IsVar="0" Zoom="0" Mutiple="1.000000" Round="0" CharSize="16" IsInputLabelL="0" IsInputLabelR="0" IsInputDefault="0" bShowRange="0" IsVar1="0" ColorHText="0x0 0" ColorHBag="0x0 0" ColorLText="0x0 0" ColorLBag="0x0 0"/>
<Extension IsCheck="0" Lockmate="0" DrawLock="0" LockMode="0" UseShowHide="0" HideType="0" IsHideAllTime="0" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
<PartInfo PartType="Text" PartName="TXT_2">
<General TextContent="Вх фильтр, 0...1024" LaFrnColor="0x0 0" IsBackColor="0" BgColor="0xffffff 0" CharSize="176 126 126 126 126 126 126 12" Bold="0" StartPt="62 82"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
<PartInfo PartType="Text" PartName="TXT_3">
<General TextContent="Kp, 0...32767" LaFrnColor="0x0 0" IsBackColor="0" BgColor="0xffffff 0" CharSize="176 126 126 126 126 126 126 12" Bold="0" StartPt="65 111"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
<PartInfo PartType="Text" PartName="TXT_4">
<General TextContent="Ki, 0...32767" LaFrnColor="0x0 0" IsBackColor="0" BgColor="0xffffff 0" CharSize="176 126 126 126 126 126 126 12" Bold="0" StartPt="67 138"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
<PartInfo PartType="Numeric" PartName="NUM_1">
<General Desc="NUM_0" Area="210 77 297 100" WordAddr="HDW8011" Fast="0" IsInput="1" WriteAddr="HDW8011" KbdScreen="1000" IsPopKeyBrod="0" FigureFile="TFT-type style\dp_zc00.pvg" BorderColor="0xcccccc 0" FrnColor="0x0 0" BgColor="0xffffff 0" BmpIndex="-1" Transparent="0" IsHideNum="0" HighZeroPad="0" IsShowPwd="0" UseGlint="0" GlintFgClr="0x0 0" ZeroNoDisplay="0" IsIndirectR="0" IsIndirectW="0" IsAddFrame="0" IsWordOrder="0"/>
<DispFormat DispType="2" DigitCount="4 0" DataLimit="0 1149239296" DataRange="0.000000 1024.000000" IsVar="0" Zoom="0" Mutiple="1.000000" Round="0" CharSize="16" IsInputLabelL="0" IsInputLabelR="0" IsInputDefault="0" bShowRange="0" IsVar1="0" ColorHText="0x0 0" ColorHBag="0x0 0" ColorLText="0x0 0" ColorLBag="0x0 0"/>
<Extension IsCheck="0" Lockmate="0" DrawLock="0" LockMode="0" UseShowHide="0" HideType="0" IsHideAllTime="0" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
<PartInfo PartType="Numeric" PartName="NUM_2">
<General Desc="NUM_0" Area="175 106 262 129" WordAddr="HDW8012" Fast="0" IsInput="1" WriteAddr="HDW8012" KbdScreen="1000" IsPopKeyBrod="0" FigureFile="TFT-type style\dp_zc00.pvg" BorderColor="0xcccccc 0" FrnColor="0x0 0" BgColor="0xffffff 0" BmpIndex="-1" Transparent="0" IsHideNum="0" HighZeroPad="0" IsShowPwd="0" UseGlint="0" GlintFgClr="0x0 0" ZeroNoDisplay="0" IsIndirectR="0" IsIndirectW="0" IsAddFrame="0" IsWordOrder="0"/>
<DispFormat DispType="2" DigitCount="5 0" DataLimit="0 1191181824" DataRange="0.000000 32767.000000" IsVar="0" Zoom="0" Mutiple="1.000000" Round="0" CharSize="16" IsInputLabelL="0" IsInputLabelR="0" IsInputDefault="0" bShowRange="0" IsVar1="0" ColorHText="0x0 0" ColorHBag="0x0 0" ColorLText="0x0 0" ColorLBag="0x0 0"/>
<Extension IsCheck="0" Lockmate="0" DrawLock="0" LockMode="0" UseShowHide="0" HideType="0" IsHideAllTime="0" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
<PartInfo PartType="Text" PartName="TXT_6">
<General TextContent="Нижняя граница, 0" LaFrnColor="0x0 0" IsBackColor="0" BgColor="0xffffff 0" CharSize="176 126 126 126 126 126 126 12" Bold="0" StartPt="65 204"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
<PartInfo PartType="Text" PartName="TXT_7">
<General TextContent="Верхняя граница, 2000" LaFrnColor="0x0 0" IsBackColor="0" BgColor="0xffffff 0" CharSize="176 126 126 126 126 126 126 12" Bold="0" StartPt="65 231"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
<PartInfo PartType="Numeric" PartName="NUM_6">
<General Desc="NUM_0" Area="238 199 325 223" WordAddr="HDW8016" Fast="0" IsInput="1" WriteAddr="HDW8016" KbdScreen="1000" IsPopKeyBrod="0" FigureFile="TFT-type style\dp_zc00.pvg" BorderColor="0xcccccc 0" FrnColor="0x0 0" BgColor="0xffffff 0" BmpIndex="-1" Transparent="0" IsHideNum="0" HighZeroPad="0" IsShowPwd="0" UseGlint="0" GlintFgClr="0x0 0" ZeroNoDisplay="0" IsIndirectR="0" IsIndirectW="0" IsAddFrame="0" IsWordOrder="0"/>
<DispFormat DispType="2" DigitCount="4 0" DataLimit="0 1157234688" DataRange="0.000000 2000.000000" IsVar="0" Zoom="0" Mutiple="1.000000" Round="0" CharSize="16" IsInputLabelL="0" IsInputLabelR="0" IsInputDefault="0" bShowRange="0" IsVar1="0" ColorHText="0x0 0" ColorHBag="0x0 0" ColorLText="0x0 0" ColorLBag="0x0 0"/>
<Extension IsCheck="0" Lockmate="0" DrawLock="0" LockMode="0" UseShowHide="0" HideType="0" IsHideAllTime="0" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
<PartInfo PartType="Numeric" PartName="NUM_7">
<General Desc="NUM_0" Area="238 226 325 250" WordAddr="HDW8017" Fast="0" IsInput="1" WriteAddr="HDW8017" KbdScreen="1000" IsPopKeyBrod="0" FigureFile="TFT-type style\dp_zc00.pvg" BorderColor="0xcccccc 0" FrnColor="0x0 0" BgColor="0xffffff 0" BmpIndex="-1" Transparent="0" IsHideNum="0" HighZeroPad="0" IsShowPwd="0" UseGlint="0" GlintFgClr="0x0 0" ZeroNoDisplay="0" IsIndirectR="0" IsIndirectW="0" IsAddFrame="0" IsWordOrder="0"/>
<DispFormat DispType="2" DigitCount="5 0" DataLimit="0 1157234688" DataRange="0.000000 2000.000000" IsVar="0" Zoom="0" Mutiple="1.000000" Round="0" CharSize="16" IsInputLabelL="0" IsInputLabelR="0" IsInputDefault="0" bShowRange="0" IsVar1="0" ColorHText="0x0 0" ColorHBag="0x0 0" ColorLText="0x0 0" ColorLBag="0x0 0"/>
<Extension IsCheck="0" Lockmate="0" DrawLock="0" LockMode="0" UseShowHide="0" HideType="0" IsHideAllTime="0" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
<PartInfo PartType="FunctionSwitch" PartName="FS_0">
<General Desc="FS_0" Area="13 230 49 257" ScrSwitch="1" ScreenNo="3" ScreenNo2="-1" PointPos="0 0" PopupScreenType="0" PopupCloseWithParent="0" FigureFile="Arrow-type style\swjt_a03.pvg" BorderColor="0xcccccc 0" Pattern="7471169" FrnColor="0x0 0" BgColor="0x0 0" BmpIndex="-1" LaStartPt="30 24" UseShowHide="0" HideType="0" IsHideAllTime="0"/>
<Extension Lockmate="0" DrawLock="0" IsShowGrayScale="0" LockMode="0" TouchState="1" Buzzer="1" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/>
<Label Status="0" Bold="0" CharSize="1414141414141414" LaFrnColor="0x0 0"/></PartInfo>
<PartInfo PartType="Numeric" PartName="NUM_3">
<General Desc="NUM_0" Area="175 133 262 157" WordAddr="HDW8013" Fast="0" IsInput="1" WriteAddr="HDW8013" KbdScreen="1000" IsPopKeyBrod="0" FigureFile="TFT-type style\dp_zc00.pvg" BorderColor="0xcccccc 0" FrnColor="0x0 0" BgColor="0xffffff 0" BmpIndex="-1" Transparent="0" IsHideNum="0" HighZeroPad="0" IsShowPwd="0" UseGlint="0" GlintFgClr="0x0 0" ZeroNoDisplay="0" IsIndirectR="0" IsIndirectW="0" IsAddFrame="0" IsWordOrder="0"/>
<DispFormat DispType="2" DigitCount="5 0" DataLimit="0 1191181824" DataRange="0.000000 32767.000000" IsVar="0" Zoom="0" Mutiple="1.000000" Round="0" CharSize="16" IsInputLabelL="0" IsInputLabelR="0" IsInputDefault="0" bShowRange="0" IsVar1="0" ColorHText="0x0 0" ColorHBag="0x0 0" ColorLText="0x0 0" ColorLBag="0x0 0"/>
<Extension IsCheck="0" Lockmate="0" DrawLock="0" LockMode="0" UseShowHide="0" HideType="0" IsHideAllTime="0" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
<PartInfo PartType="Numeric" PartName="NUM_4">
<General Desc="NUM_0" Area="175 160 262 184" WordAddr="HDW8014" Fast="0" IsInput="1" WriteAddr="HDW8014" KbdScreen="1000" IsPopKeyBrod="0" FigureFile="TFT-type style\dp_zc00.pvg" BorderColor="0xcccccc 0" FrnColor="0x0 0" BgColor="0xffffff 0" BmpIndex="-1" Transparent="0" IsHideNum="0" HighZeroPad="0" IsShowPwd="0" UseGlint="0" GlintFgClr="0x0 0" ZeroNoDisplay="0" IsIndirectR="0" IsIndirectW="0" IsAddFrame="0" IsWordOrder="0"/>
<DispFormat DispType="2" DigitCount="5 0" DataLimit="0 1191181824" DataRange="0.000000 32767.000000" IsVar="0" Zoom="0" Mutiple="1.000000" Round="0" CharSize="16" IsInputLabelL="0" IsInputLabelR="0" IsInputDefault="0" bShowRange="0" IsVar1="0" ColorHText="0x0 0" ColorHBag="0x0 0" ColorLText="0x0 0" ColorLBag="0x0 0"/>
<Extension IsCheck="0" Lockmate="0" DrawLock="0" LockMode="0" UseShowHide="0" HideType="0" IsHideAllTime="0" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
<PartInfo PartType="BitSwitch" PartName="BS_0">
<General Desc="BS_0" Area="374 23 461 46" OperateAddr="HDX10.2" Fast="0" BitFunc="2" ResetDelay="100" Monitor="1" MonitorAddr="HDX10.2" FigureFile="TFT-type style\TFT001.pvg" BorderColor="0xcccccc 0" BmpIndex="-1" Align="3" LaStartPt="8 5" BitShowReverse="0" UseGlint="0" UseShowHide="0" HideType="0" IsHideAllTime="0" IsIndirectR="0" IsIndirectW="0"/>
<Extension IsCheck="0" Lockmate="0" DrawLock="0" IsShowGrayScale="0" LockMode="0" TouchState="1" Buzzer="1" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/>
<Label Status="0" Pattern="1" FrnColor="0xffffff 1" BgColor="0xffffff 0" Bold="0" LaIndexID="По умолчанию" CharSize="2214141414141414" LaFrnColor="0x0 0"/>
<Label Status="1" Pattern="1" FrnColor="0xffffff 0" BgColor="0xffffff 0" Bold="0" LaIndexID="По умолчанию" CharSize="2214141414141414" LaFrnColor="0x0 0"/></PartInfo></ScrInfo>
