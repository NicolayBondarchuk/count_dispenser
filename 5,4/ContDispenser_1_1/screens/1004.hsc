<?xml version="1.0" encoding="utf-8"?>
<ScrInfo ScreenNo="0" ScreenType="" ScreenSize="0">
	<PartInfo PartType="keystoke" PartName="KY_0">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="42 100 79 134"/>
		<Key ASCIIKey="A"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="AAAAAAAA" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_1">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="193 134 230 168"/>
		<Key ASCIIKey="B"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="BBBBBBBB" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_2">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="117 134 154 168"/>
		<Key ASCIIKey="C"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="CCCCCCCC" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_3">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="117 100 154 134"/>
		<Key ASCIIKey="D"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="DDDDDDDD" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_4">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="117 65 154 99"/>
		<Key ASCIIKey="E"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="EEEEEEEE" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_5">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="155 100 192 134"/>
		<Key ASCIIKey="F"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="FFFFFFFF" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_7">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="231 100 268 134"/>
		<Key ASCIIKey="H"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="HHHHHHHH" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_8">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="306 65 343 99"/>
		<Key ASCIIKey="I"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="IIIIIIII" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_9">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="268 100 306 134"/>
		<Key ASCIIKey="J"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="JJJJJJJJ" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_10">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="306 100 343 134"/>
		<Key ASCIIKey="K"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="KKKKKKKK" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_11">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="344 100 381 134"/>
		<Key ASCIIKey="L"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="LLLLLLLL" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_13">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="231 134 268 168"/>
		<Key ASCIIKey="N"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="NNNNNNNN" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_8 16">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="344 65 381 99"/>
		<Key ASCIIKey="O"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="OOOOOOOO" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_15">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="382 65 419 99"/>
		<Key ASCIIKey="P"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="PPPPPPPP" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_16">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="42 65 79 99"/>
		<Key ASCIIKey="Q"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="QQQQQQQQ" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_17">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="155 65 192 99"/>
		<Key ASCIIKey="R"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="RRRRRRRR" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_19">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="193 65 230 99"/>
		<Key ASCIIKey="T"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="TTTTTTTT" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_20">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="268 65 306 99"/>
		<Key ASCIIKey="U"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="UUUUUUUU" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_21">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="10 10" Area="344 134 419 168"/>
		<Key IsCtrlKey="1" ASCIIKey="A"/>
		<Label Status="0" LaFrnColor="0xffff99 -1" LaIndexID="ENTENTENTENTENTENTENTENT" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_23">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="306 134 343 168"/>
		<Key IsCtrlKey="1" CtrlKey="2" ASCIIKey="A"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="CLRCLRCLRCLRCLRCLRCLRCLR" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_24">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="382 100 419 134"/>
		<Key IsCtrlKey="1" CtrlKey="1" ASCIIKey="A"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="&lt;--&lt;--&lt;--&lt;--&lt;--&lt;--&lt;--&lt;--" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_0">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="155 134 192 168"/>
		<Key ASCIIKey="V"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="VVVVVVVV" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_1">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="79 65 117 99"/>
		<Key ASCIIKey="W"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="WWWWWWWW" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_2">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="79 134 117 168"/>
		<Key ASCIIKey="X"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="XXXXXXXX" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_4">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="42 134 79 168"/>
		<Key ASCIIKey="Z"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="ZZZZZZZZ" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="String" PartName="STR_0">
		<General Desc="STR_0" WordAddr="HSW160" stCount="15" IsInput="1" WriteAddr="HSW160" KbdScreen="-1" BorderColor="0xc0 16777215" FrnColor="0x0 -1" BgColor="0xcccccc -1" CharSize="16 32" IsShowPwd="1" Area="6 2 412 26"/>
		<Extension/>
		<MoveZoom DataFormatMZ="2"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_6">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="193 30 230 64"/>
		<Key ASCIIKey="5"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="55555555" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_7">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="117 30 154 64"/>
		<Key ASCIIKey="3"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="33333333" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_8">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="79 30 117 64"/>
		<Key ASCIIKey="2"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="22222222" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_9">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="42 30 79 64"/>
		<Key ASCIIKey="1"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="11111111" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_10">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="382 30 419 64"/>
		<Key ASCIIKey="0"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="00000000" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_13">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="344 30 381 64"/>
		<Key ASCIIKey="9"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="99999999" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_8 16">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="306 30 343 64"/>
		<Key ASCIIKey="8"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="88888888" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_15">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="268 30 306 64"/>
		<Key ASCIIKey="7"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="77777777" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_16">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="231 30 268 64"/>
		<Key ASCIIKey="6"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="66666666" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="BitSwitch" PartName="BS_0">
		<General Desc="BS_0" OperateAddr="HSX216.00" BitFunc="3" Monitor="1" MonitorAddr="HSX216.00" FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="7 9" Area="0 100 41 134"/>
		<Extension AckTime="20" TouchState="1" Buzzer="1"/>
		<MoveZoom DataFormatMZ="2"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="CAPCAPCAPCAPCAPCAPCAPCAP" CharSize="8 168 168 168 168 168 168 168 16"/>
		<Label Status="1" LaIndexID="CAPCAPCAPCAPCAPCAPCAPCAP" CharSize="8 168 168 168 168 168 168 168 16" LaFrnColor="0xffff99 -1"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_6">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="193 100 230 134"/>
		<Key ASCIIKey="G"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="GGGGGGGG" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_12">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="268 134 306 168"/>
		<Key ASCIIKey="M"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="MMMMMMMM" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_18">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="79 100 117 134"/>
		<Key ASCIIKey="S"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="SSSSSSSS" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_22">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="6 31" Area="0 28 41 101"/>
		<Key IsCtrlKey="1" CtrlKey="3" ASCIIKey="A"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="ESCESCESCESCESCESCESCESC" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_3">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="231 65 268 99"/>
		<Key ASCIIKey="Y"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="YYYYYYYY" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_5">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="13 5" Area="155 30 192 64"/>
		<Key ASCIIKey="4"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="44444444" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_11">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="7 8" Area="0 134 41 168"/>
		<Key ASCIIKey=" "/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="SPASPASPASPASPASPASPASPA" CharSize="8 168 168 168 168 168 168 168 16"/></PartInfo></ScrInfo>
