<?xml version="1.0" encoding="UTF-8"?>
<ScrInfo ScreenNo="8" ScreenType="" ScreenSize="0">
	<Script>
		<TrigAction>
			<Trigger Action="1" BitAddr="S4">@W_D16=@W_HDW8004 * 20		' * 2000 / 100%
@W_D5=@W_HDW8005*10
@W_D6=@W_HDW8006*10
@W_D7=@W_HDW8007

' X1
@W_HDW8030 = W2F(@W_D64) ' + @W_D65 * 65536)

' Y1
@W_HDW8034 = W2F(@W_HDW8008) * 3.6 / W2F(@W_HDW8002)


'подтягивание к нулю
dim x, y, k1 as floating

x = asFloating(@W_HDW8030)

y = asFloating(@W_HDW8034)

k1 = x / y

@W_HDW8048 = k1
</Trigger>
			<Trigger Action="0" BitAddr="X1">@B_M46 = 1
</Trigger>
			<Trigger Action="1" BitAddr="S9">' X2
@W_HDW8032 = W2F(@W_D64) ' + @W_D65 * 65536)

'Y2
@W_HDW8036 = W2F(@W_HDW8008) * 3.6 / W2F(@W_HDW8006)




dim x1, x2, y1, y2, k, b as floating

x1 = asFloating(@W_HDW8030)
x2 = asFloating(@W_HDW8032)
y1 = asFloating(@W_HDW8034)
y2 = asFloating(@W_HDW8036)

k = (x2 - x1) / (y2 - y1)

b = x1  / (y2 - y1) * y2 - x2 / (y2 - y1)  * y1  '(x1 * y2 - y1 * x2) / (y2 - y1)

@W_HDW8044 = k
@W_HDW8046 = b
</Trigger>
			<Trigger Action="1" BitAddr="X1">@B_M46 = 0
</Trigger>
			<Trigger Action="1" BitAddr="X0">@B_M40 = 1
</Trigger>
		</TrigAction>
		<InitialAction>@W_D19 = 0   ' режимы калибровки
@W_D9 = 0		' режимы калибровки основные
@B_M37 = 1		' переход на экран калибровки, запуск S20
@B_M33 = 1		' видимость кнопки ПУСК

@W_D16=@W_HDW8000 * 20		' (* 2000 / 100%) производительность лотка
@W_D5=@W_HDW8001*10			' время для формирования нужного слоя
@W_D6=@W_HDW8002*10			' время калибровки
@W_D7=@W_HDW8003			' время дискретизации
</InitialAction>
		<CloseAction>@B_M37 = 0
@W_D8 = 0
@W_D19 = 0
</CloseAction></Script>
<PartInfo PartType="WordShow" PartName="WL_0">
<General Desc="WL_0" Area="37 93 402 119" WordAddr="D19" StatsNum="10" Fast="0" DataFormat="2" FigureFile="" BorderColor="0xcccccc 0" BmpIndex="-1" Align="3" LaStartPt="0 10" StatusCovType="0" AnimaReturn="0" ByAddr="0" Trigger="0" UseShowHide="0" HideType="0" IsHideAllTime="0" IsIndirectR="0" IsIndirectW="0" isNautomatic="0" IsCtrlStaTextByAddr="0" isReturn="0" isStateControl="0"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/>
<Extension IsCheck="0" Lockmate="0" DrawLock="0" IsShowGrayScale="0" LockMode="0" TouchState="1" Buzzer="1" IsUesPartPassword="0" IsSetLowerLev="0"/>
<Label Status="0" FrnColor="0xffffff 1" BgColor="0xffffff 0" Bold="0" LaIndexID="Нажмите кнопку &quot;ПУСК&quot;" CharSize="1714141414141414" LaFrnColor="0x0 0" UseGlint="0" GlintFgClr="0x0 0"/>
<Label Status="1" Pattern="1" FrnColor="0xffffff 7471188" BgColor="0xffffff 0" Bold="0" LaIndexID="Идет формирование слоя продукта" CharSize="176 126 126 126 126 126 126 12" LaFrnColor="0x0 0" UseGlint="0" GlintFgClr="0x0 0"/>
<Label Status="2" Pattern="7471188" FrnColor="0xffffff 7471188" BgColor="0xffffff 0" Bold="0" LaIndexID="Установите тару и нажмите &quot;ДАЛЕЕ&quot;" CharSize="176 126 126 126 126 126 126 12" LaFrnColor="0x0 0" UseGlint="0" GlintFgClr="0x0 0"/>
<Label Status="3" Pattern="7471188" FrnColor="0xffffff 0" BgColor="0xffffff 0" Bold="0" LaIndexID="Идет калибровка низкого расхода" CharSize="176 126 126 126 126 126 126 12" LaFrnColor="0x0 0" UseGlint="0" GlintFgClr="0x0 0"/>
<Label Status="4" FrnColor="0x0 0" BgColor="0x0 0" Bold="0" LaIndexID="Идет калибровка высокого расхода" CharSize="176 126 126 126 126 126 126 12" LaFrnColor="0x0 0" UseGlint="0" GlintFgClr="0x0 0"/>
<Label Status="5" FrnColor="0x0 0" BgColor="0x0 0" Bold="0" LaIndexID="Введите полученный вес и нажмите ДАЛЕЕ" CharSize="176 126 126 126 126 126 126 12" LaFrnColor="0x0 0" UseGlint="0" GlintFgClr="0x0 0"/>
<Label Status="6" FrnColor="0x0 0" BgColor="0x0 0" Bold="0" LaIndexID="Калибровка низкого расхода произведена " CharSize="176 126 126 126 126 126 126 12" LaFrnColor="0x0 0" UseGlint="0" GlintFgClr="0x0 0"/>
<Label Status="7" FrnColor="0x0 0" BgColor="0x0 0" Bold="0" LaIndexID="Калибровка высокого расхода произведена " CharSize="176 126 126 126 126 126 126 12" LaFrnColor="0x0 0" UseGlint="0" GlintFgClr="0x0 0"/>
<Label Status="8" FrnColor="0x0 0" BgColor="0x0 0" Bold="0" LaIndexID="Идет расчет коэффициентов" CharSize="176 126 126 126 126 126 126 12" LaFrnColor="0x0 0" UseGlint="0" GlintFgClr="0x0 0"/>
<Label Status="9" FrnColor="0x0 0" BgColor="0x0 0" Bold="0" LaIndexID="ГОТОВО" CharSize="176 126 126 126 126 126 126 12" LaFrnColor="0x0 0" UseGlint="0" GlintFgClr="0x0 0"/></PartInfo>
<PartInfo PartType="Rect" PartName="REC_0">
<General Area="9 5 241 33" BorderColor="0x0 0" Pattern="1" FrnColor="0xc0c0c0 -1" BgColor="0x80ff -1" ActiveColor="0"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
<PartInfo PartType="Text" PartName="TXT_0">
<General TextContent="КАЛИБРОВКА ПРОДУКТА" LaFrnColor="0x0 0" IsBackColor="0" BgColor="0xffffff 0" CharSize="166 126 126 126 126 126 126 12" Bold="0" StartPt="16 9"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
<PartInfo PartType="BitSwitch" PartName="BS_0">
<General Desc="BS_0" Area="39 132 97 161" OperateAddr="M40" Fast="0" BitFunc="2" ResetDelay="100" Monitor="1" MonitorAddr="M40" FigureFile="TFT-type style\TFT001.pvg" BorderColor="0xcccccc 0" BmpIndex="-1" Align="3" LaStartPt="17 8" BitShowReverse="0" UseGlint="0" UseShowHide="1" TrigHideAddr="M33" HideType="0" IsHideAllTime="0" IsIndirectR="0" IsIndirectW="0"/>
<Extension IsCheck="0" Lockmate="0" DrawLock="0" IsShowGrayScale="0" LockMode="0" TouchState="1" Buzzer="1" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/>
<Label Status="0" Pattern="1" FrnColor="0xffffff 1" BgColor="0xffffff 0" Bold="0" LaIndexID="ПУСК" CharSize="2214141414141414" LaFrnColor="0x0 0"/>
<Label Status="1" Pattern="1" FrnColor="0xffffff 0" BgColor="0xffffff 0" Bold="0" LaIndexID="ПУСК" CharSize="2214141414141414" LaFrnColor="0x0 0"/></PartInfo>
<PartInfo PartType="BitSwitch" PartName="BS_1">
<General Desc="BS_0" Area="255 133 313 162" OperateAddr="M42" Fast="0" BitFunc="3" Monitor="1" MonitorAddr="M42" FigureFile="TFT-type style\TFT001.pvg" BorderColor="0xcccccc 0" BmpIndex="-1" Align="3" LaStartPt="12 9" BitShowReverse="0" UseGlint="0" UseShowHide="1" TrigHideAddr="M31" HideType="0" IsHideAllTime="0" IsIndirectR="0" IsIndirectW="0"/>
<Extension IsCheck="0" Lockmate="0" DrawLock="0" IsShowGrayScale="0" LockMode="0" TouchState="1" Buzzer="1" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/>
<Label Status="0" Pattern="1" FrnColor="0xffffff 1" BgColor="0xffffff 0" Bold="0" LaIndexID="ДАЛЕЕ" CharSize="2214141414141414" LaFrnColor="0x0 0"/>
<Label Status="1" Pattern="1" FrnColor="0xffffff 0" BgColor="0xffffff 0" Bold="0" LaIndexID="ДАЛЕЕ" CharSize="2214141414141414" LaFrnColor="0x0 0"/></PartInfo>
<PartInfo PartType="Numeric" PartName="NUM_0">
<General Desc="NUM_0" Area="165 132 249 162" WordAddr="HDW8008" Fast="0" IsInput="1" WriteAddr="HDW8008" KbdScreen="1000" IsPopKeyBrod="0" FigureFile="TFT-type style\dp_zc00.pvg" BorderColor="0xcccccc 0" FrnColor="0x0 0" BgColor="0xffffff 0" BmpIndex="-1" Transparent="0" IsHideNum="0" HighZeroPad="0" IsShowPwd="0" UseGlint="0" GlintType="1" Glintfrq="10" GlintFgClr="0x0 0" ZeroNoDisplay="0" IsIndirectR="0" IsIndirectW="0" IsAddFrame="0" IsWordOrder="0"/>
<DispFormat DispType="2" DigitCount="4 0" DataLimit="0 1176255488" DataRange="0.000000 9999.000000" IsVar="0" Zoom="0" Mutiple="1.000000" Round="0" CharSize="17" IsInputLabelL="0" IsInputLabelR="0" IsInputDefault="0" bShowRange="0" IsVar1="0" ColorHText="0x0 0" ColorHBag="0x0 0" ColorLText="0x0 0" ColorLBag="0x0 0"/>
<Extension IsCheck="0" Lockmate="0" DrawLock="0" LockMode="0" UseShowHide="1" TrigHideAddr="M31" HideType="0" IsHideAllTime="0" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
<PartInfo PartType="BitSwitch" PartName="BS_2">
<General Desc="BS_0" Area="101 133 158 162" OperateAddr="M41" Fast="0" BitFunc="2" ResetDelay="100" Monitor="1" MonitorAddr="M41" FigureFile="TFT-type style\TFT001.pvg" BorderColor="0xcccccc 0" BmpIndex="-1" Align="3" LaStartPt="11 8" BitShowReverse="0" UseGlint="0" UseShowHide="1" TrigHideAddr="M30" HideType="0" IsHideAllTime="0" IsIndirectR="0" IsIndirectW="0"/>
<Extension IsCheck="0" Lockmate="0" DrawLock="0" IsShowGrayScale="0" LockMode="0" TouchState="1" Buzzer="1" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/>
<Label Status="0" Pattern="1" FrnColor="0xffffff 1" BgColor="0xffffff 0" Bold="0" LaIndexID="ДАЛЕЕ" CharSize="2214141414141414" LaFrnColor="0x0 0"/>
<Label Status="1" Pattern="1" FrnColor="0xffffff 0" BgColor="0xffffff 0" Bold="0" LaIndexID="ДАЛЕЕ" CharSize="2214141414141414" LaFrnColor="0x0 0"/></PartInfo>
<PartInfo PartType="BitSwitch" PartName="BS_3">
<General Desc="BS_0" Area="330 132 430 161" OperateAddr="M46" Fast="0" BitFunc="3" Monitor="1" MonitorAddr="M46" FigureFile="TFT-type style\TFT001.pvg" BorderColor="0xcccccc 0" BmpIndex="-1" LaStartPt="28 8" BitShowReverse="0" UseGlint="0" UseShowHide="0" HideType="0" IsHideAllTime="0" IsIndirectR="0" IsIndirectW="0"/>
<Extension IsCheck="0" Lockmate="0" DrawLock="0" IsShowGrayScale="0" LockMode="0" TouchState="1" Buzzer="1" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/>
<Label Status="0" Pattern="1" FrnColor="0xffffff 1" BgColor="0xffffff 0" Bold="0" LaIndexID="ОТМЕНА" CharSize="2214141414141414" LaFrnColor="0x0 0"/>
<Label Status="1" Pattern="1" FrnColor="0xffffff 0" BgColor="0xffffff 0" Bold="0" LaIndexID="ОТМЕНА" CharSize="2214141414141414" LaFrnColor="0x0 0"/></PartInfo>
<PartInfo PartType="FunctionSwitch" PartName="FS_0">
<General Desc="FS_0" Area="13 230 49 257" ScrSwitch="1" ScreenNo="3" ScreenNo2="-1" PointPos="0 0" PopupScreenType="0" PopupCloseWithParent="0" FigureFile="Arrow-type style\swjt_a03.pvg" BorderColor="0xcccccc 0" Pattern="7471169" FrnColor="0x0 0" BgColor="0x0 0" BmpIndex="-1" LaStartPt="30 24" UseShowHide="0" HideType="0" IsHideAllTime="0"/>
<Extension Lockmate="0" DrawLock="0" IsShowGrayScale="0" LockMode="0" TouchState="1" Buzzer="1" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/>
<Label Status="0" Bold="0" CharSize="1414141414141414" LaFrnColor="0x0 0"/></PartInfo>
<PartInfo PartType="FunctionSwitch" PartName="FS_1">
<General Desc="FS_0" Area="373 6 460 32" ScrSwitch="1" ScreenNo="9" ScreenNo2="-1" PointPos="0 0" PopupScreenType="0" PopupCloseWithParent="0" FigureFile="TFT-type style\TFT001.pvg" BorderColor="0xcccccc 0" FrnColor="0x0 0" BgColor="0x0 0" BmpIndex="-1" Align="3" LaStartPt="12 8" UseShowHide="0" HideType="0" IsHideAllTime="0"/>
<Extension Lockmate="0" DrawLock="0" IsShowGrayScale="0" LockMode="0" TouchState="1" Buzzer="1" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/>
<Label Status="0" Bold="0" LaIndexID="НАСТРОЙКИ" CharSize="2216161616161616" LaFrnColor="0x0 0"/></PartInfo>
<PartInfo PartType="Numeric" PartName="NUM_2">
<General Desc="NUM_0" Area="97 235 171 265" WordAddr="HDW8044" Fast="0" nuCount="1" IsInput="0" WriteAddr="HDW8044" KbdScreen="1000" IsPopKeyBrod="0" FigureFile="TFT-type style\dp_zc00.pvg" BorderColor="0xcccccc 0" FrnColor="0x0 0" BgColor="0xffffff 0" BmpIndex="-1" Transparent="0" IsHideNum="0" HighZeroPad="0" IsShowPwd="0" UseGlint="0" GlintType="1" Glintfrq="10" GlintFgClr="0x0 0" ZeroNoDisplay="0" IsIndirectR="0" IsIndirectW="0" IsAddFrame="0" IsWordOrder="0"/>
<DispFormat DispType="5" DigitCount="5 3" DataLimit="0 1203982336" DataRange="0.000000 99999.999000" IsVar="0" Zoom="0" Mutiple="1.000000" Round="0" CharSize="17" IsInputLabelL="0" IsInputLabelR="0" IsInputDefault="0" bShowRange="0" IsVar1="0" ColorHText="0x0 0" ColorHBag="0x0 0" ColorLText="0x0 0" ColorLBag="0x0 0"/>
<Extension IsCheck="0" Lockmate="0" DrawLock="0" LockMode="0" UseShowHide="0" HideType="0" IsHideAllTime="0" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
<PartInfo PartType="Text" PartName="TXT_2">
<General TextContent="k" LaFrnColor="0x0 0" IsBackColor="0" BgColor="0xffffff 0" CharSize="166 126 126 126 126 126 126 12" Bold="0" StartPt="79 241"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
<PartInfo PartType="WordShow" PartName="WL_1">
<General Desc="WL_0" Area="37 59 402 85" WordAddr="D8" StatsNum="4" Fast="0" DataFormat="2" FigureFile="" BorderColor="0xcccccc 0" BmpIndex="-1" Align="3" LaStartPt="0 10" StatusCovType="0" AnimaReturn="0" ByAddr="0" Trigger="0" UseShowHide="0" HideType="0" IsHideAllTime="0" IsIndirectR="0" IsIndirectW="0" isNautomatic="0" IsCtrlStaTextByAddr="0" isReturn="0" isStateControl="0"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/>
<Extension IsCheck="0" Lockmate="0" DrawLock="0" IsShowGrayScale="0" LockMode="0" TouchState="1" Buzzer="1" IsUesPartPassword="0" IsSetLowerLev="0"/>
<Label Status="0" FrnColor="0xffffff 1" BgColor="0xffffff 0" Bold="0" LaIndexID="КАЛИБРОВКА" CharSize="1614141414141414" LaFrnColor="0x0 0" UseGlint="0" GlintFgClr="0x0 0"/>
<Label Status="1" Pattern="1" FrnColor="0xffffff 7471188" BgColor="0xffffff 0" Bold="0" LaIndexID="КАЛИБРОВКА НИЗКОГО РАСХОДА" CharSize="166 126 126 126 126 126 126 12" LaFrnColor="0x0 0" UseGlint="0" GlintFgClr="0x0 0"/>
<Label Status="2" Pattern="7471188" FrnColor="0xffffff 7471188" BgColor="0xffffff 0" Bold="0" LaIndexID="КАЛИБРОВКА ВЫСОКОГО РАСХОДА" CharSize="176 126 126 126 126 126 126 12" LaFrnColor="0x0 0" UseGlint="0" GlintFgClr="0x0 0"/>
<Label Status="3" Pattern="7471188" FrnColor="0xffffff 0" BgColor="0xffffff 0" Bold="0" LaIndexID="КАЛИБРОВКА ПРОИЗВЕДЕНА" CharSize="6 126 126 126 126 126 126 126 12" LaFrnColor="0x0 0" UseGlint="0" GlintFgClr="0x0 0"/></PartInfo>
<PartInfo PartType="Text" PartName="TXT_1">
<General TextContent="b" LaFrnColor="0x0 0" IsBackColor="0" BgColor="0xffffff 0" CharSize="166 126 126 126 126 126 126 12" Bold="0" StartPt="187 241"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
<PartInfo PartType="Numeric" PartName="NUM_1">
<General Desc="NUM_0" Area="212 235 290 265" WordAddr="HDW8046" Fast="0" nuCount="1" IsInput="0" WriteAddr="HDW8046" KbdScreen="1000" IsPopKeyBrod="0" FigureFile="TFT-type style\dp_zc00.pvg" BorderColor="0xcccccc 0" FrnColor="0x0 0" BgColor="0xffffff 0" BmpIndex="-1" Transparent="0" IsHideNum="0" HighZeroPad="0" IsShowPwd="0" UseGlint="0" GlintType="1" Glintfrq="10" GlintFgClr="0x0 0" ZeroNoDisplay="0" IsIndirectR="0" IsIndirectW="0" IsAddFrame="0" IsWordOrder="0"/>
<DispFormat DispType="5" DigitCount="5 3" DataLimit="0 1203982336" DataRange="0.000000 99999.999000" IsVar="0" Zoom="0" Mutiple="1.000000" Round="0" CharSize="17" IsInputLabelL="0" IsInputLabelR="0" IsInputDefault="0" bShowRange="0" IsVar1="0" ColorHText="0x0 0" ColorHBag="0x0 0" ColorLText="0x0 0" ColorLBag="0x0 0"/>
<Extension IsCheck="0" Lockmate="0" DrawLock="0" LockMode="0" UseShowHide="0" HideType="0" IsHideAllTime="0" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
<PartInfo PartType="Rect" PartName="REC_1">
<General Area="167 171 472 209" BorderColor="0x0 0" Pattern="1" FrnColor="0xffffff -1" BgColor="0xffffff 0" ActiveColor="0"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
<PartInfo PartType="Numeric" PartName="NUM_3">
<General Desc="NUM_0" Area="336 175 459 206" WordAddr="D0" Fast="0" IsInput="0" WriteAddr="D0" KbdScreen="1000" IsPopKeyBrod="0" FigureFile="TFT-type style\dp_zc00.pvg" BorderColor="0xcccccc 0" FrnColor="0x0 0" BgColor="0xffffff 0" BmpIndex="-1" Transparent="0" IsHideNum="0" HighZeroPad="0" IsShowPwd="0" UseGlint="0" GlintFgClr="0x0 0" ZeroNoDisplay="0" IsIndirectR="0" IsIndirectW="0" IsAddFrame="0" IsWordOrder="0"/>
<DispFormat DispType="6" DigitCount="5 0" DataLimit="3296313344 1191181824" DataRange="-999.000000 32767.000000" IsVar="0" Zoom="0" Mutiple="1.000000" Round="0" CharSize="17" IsInputLabelL="0" IsInputLabelR="0" IsInputDefault="0" bShowRange="0" IsVar1="0" ColorHText="0x0 0" ColorHBag="0x0 0" ColorLText="0x0 0" ColorLBag="0x0 0"/>
<Extension IsCheck="0" Lockmate="0" DrawLock="0" LockMode="0" UseShowHide="0" HideType="0" IsHideAllTime="0" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
<PartInfo PartType="Text" PartName="TXT_3">
<General TextContent="Показания тензодатчика" LaFrnColor="0x0 0" IsBackColor="0" BgColor="0xffffff 0" CharSize="176 126 126 126 126 126 126 12" Bold="0" StartPt="181 184"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
<PartInfo PartType="FunctionSwitch" PartName="FS_2">
<General Desc="FS_0" Area="419 231 455 258" ScrSwitch="1" ScreenNo="11" ScreenNo2="-1" PointPos="0 0" PopupScreenType="0" PopupCloseWithParent="0" FigureFile="Arrow-type style\swjt_a04.pvg" BorderColor="0xcccccc 0" Pattern="7471169" FrnColor="0x0 0" BgColor="0x0 0" BmpIndex="-1" LaStartPt="18 13" UseShowHide="0" HideType="0" IsHideAllTime="0"/>
<Extension Lockmate="0" DrawLock="0" IsShowGrayScale="0" LockMode="0" TouchState="1" Buzzer="1" IsUesPartPassword="1" IsSetLowerLev="0" IsUseUserAuthority="0"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/>
<Label Status="0" Bold="0" CharSize="1414141414141414" LaFrnColor="0x0 0"/></PartInfo>
<PartInfo PartType="Numeric" PartName="NUM_4">
<General Desc="NUM_0" Area="323 235 405 265" WordAddr="HDW8048" Fast="0" nuCount="1" IsInput="0" WriteAddr="HDW8048" KbdScreen="1000" IsPopKeyBrod="0" FigureFile="TFT-type style\dp_zc00.pvg" BorderColor="0xcccccc 0" FrnColor="0x0 0" BgColor="0xffffff 0" BmpIndex="-1" Transparent="0" IsHideNum="0" HighZeroPad="0" IsShowPwd="0" UseGlint="0" GlintType="1" Glintfrq="10" GlintFgClr="0x0 0" ZeroNoDisplay="0" IsIndirectR="0" IsIndirectW="0" IsAddFrame="0" IsWordOrder="0"/>
<DispFormat DispType="5" DigitCount="5 3" DataLimit="0 1203982336" DataRange="0.000000 99999.999000" IsVar="0" Zoom="0" Mutiple="1.000000" Round="0" CharSize="17" IsInputLabelL="0" IsInputLabelR="0" IsInputDefault="0" bShowRange="0" IsVar1="0" ColorHText="0x0 0" ColorHBag="0x0 0" ColorLText="0x0 0" ColorLBag="0x0 0"/>
<Extension IsCheck="0" Lockmate="0" DrawLock="0" LockMode="0" UseShowHide="0" HideType="0" IsHideAllTime="0" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
<PartInfo PartType="Text" PartName="TXT_4">
<General TextContent="k1" LaFrnColor="0x0 0" IsBackColor="0" BgColor="0xffffff 0" CharSize="166 126 126 126 126 126 126 12" Bold="0" StartPt="298 241"/>
<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo></ScrInfo>
