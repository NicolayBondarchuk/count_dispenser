<?xml version="1.0" encoding="utf-8"?>
<ScrInfo ScreenNo="1000" ScreenType="" ScreenSize="1">
	<Script>
		<InitialAction/>
		<CloseAction/>
	</Script>
	<PartInfo PartType="String" PartName="STR_2">
		<General Desc="STR_1" WordAddr="HSW695" Fast="0" stCount="16" IsInput="0" WriteAddr="HSW695" KbdScreen="1001" IsPopKeyBrod="0" FigureFile="" BorderColor="0xcccccc 0" FrnColor="0x0 0" BgColor="0xcccccc 0" CharSize="8 16" IsHideNum="0" Transparent="0" IsShowPwd="0" IsIndirectR="0" IsIndirectW="0" IsInputDefault="0" IsDWord="0" IsHiLowRever="0" Area="233 20 351 34"/>
		<Extension IsCheck="0" Lockmate="0" DrawLock="0" LockMode="0" UseShowHide="0" HideType="0" IsHideAllTime="0" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="String" PartName="STR_1">
		<General Desc="STR_1" WordAddr="HSW687" Fast="0" stCount="16" IsInput="0" WriteAddr="HSW687" KbdScreen="1001" IsPopKeyBrod="0" FigureFile="" BorderColor="0xcccccc 0" FrnColor="0x0 0" BgColor="0xcccccc -1" CharSize="8 16" IsHideNum="0" Transparent="0" IsShowPwd="0" IsIndirectR="0" IsIndirectW="0" IsInputDefault="0" IsDWord="0" IsHiLowRever="0" Area="46 20 165 34"/>
		<Extension IsCheck="0" Lockmate="0" DrawLock="0" LockMode="0" UseShowHide="0" HideType="0" IsHideAllTime="0" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_10">
		<General Desc="KY_0" FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 16777215" BmpIndex="-1" Align="3" LaStartPt="63 10" Area="0 39 84 66"/>
		<Key IsCtrlKey="0" ASCIIKey="7" IsInputMethod="0"/>
		<Label Status="0" Bold="0" LaIndexID="77777777" CharSize="8 168 168 168 168 168 168 1614" LaFrnColor="0xffffff -1"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_0">
		<General Desc="KY_0" FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 16777215" BmpIndex="-1" LaStartPt="8 3" Area="82 39 167 66"/>
		<Key IsCtrlKey="0" ASCIIKey="8" IsInputMethod="0"/>
		<Label Status="0" Bold="0" LaIndexID="88888888" CharSize="8 168 168 168 168 168 168 1614" LaFrnColor="0xffffff -1"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_1">
		<General Desc="KY_0" FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 16777215" BmpIndex="-1" LaStartPt="8 3" Area="165 39 250 66"/>
		<Key IsCtrlKey="0" ASCIIKey="9" IsInputMethod="0"/>
		<Label Status="0" Bold="0" LaIndexID="99999999" CharSize="8 168 168 168 168 168 168 1614" LaFrnColor="0xffffff -1"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_2">
		<General Desc="KY_0" FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 16777215" BmpIndex="-1" LaStartPt="8 3" Area="0 66 84 92"/>
		<Key IsCtrlKey="0" ASCIIKey="4" IsInputMethod="0"/>
		<Label Status="0" Bold="0" LaIndexID="44444444" CharSize="8 168 168 168 168 168 168 1614" LaFrnColor="0xffffff -1"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_3">
		<General Desc="KY_0" FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 16777215" BmpIndex="-1" LaStartPt="8 3" Area="82 66 167 92"/>
		<Key IsCtrlKey="0" ASCIIKey="5" IsInputMethod="0"/>
		<Label Status="0" Bold="0" LaIndexID="55555555" CharSize="8 168 168 168 168 168 168 1614" LaFrnColor="0xffffff -1"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_4">
		<General Desc="KY_0" FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="8 3" Area="165 66 250 92"/>
		<Key IsCtrlKey="0" ASCIIKey="6" IsInputMethod="0"/>
		<Label Status="0" Bold="0" LaIndexID="66666666" CharSize="8 168 168 168 168 168 168 1614" LaFrnColor="0xffffff -1"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_5">
		<General Desc="KY_0" FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 16777215" BmpIndex="-1" LaStartPt="8 3" Area="0 92 84 120"/>
		<Key IsCtrlKey="0" ASCIIKey="1" IsInputMethod="0"/>
		<Label Status="0" Bold="0" LaIndexID="11111111" CharSize="8 168 168 168 168 168 168 1614" LaFrnColor="0xffffff -1"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_6">
		<General Desc="KY_0" FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 16777215" BmpIndex="-1" LaStartPt="8 3" Area="82 92 167 120"/>
		<Key IsCtrlKey="0" ASCIIKey="2" IsInputMethod="0"/>
		<Label Status="0" Bold="0" LaIndexID="22222222" CharSize="8 168 168 168 168 168 168 1614" LaFrnColor="0xffffff -1"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_9">
		<General Desc="KY_0" FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="8 3" Area="0 120 167 146"/>
		<Key IsCtrlKey="0" ASCIIKey="0" IsInputMethod="0"/>
		<Label Status="0" Bold="0" LaIndexID="00000000" CharSize="8 168 168 168 168 168 168 1614" LaFrnColor="0xffffff -1"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_11">
		<General Desc="KY_0" FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="22 9" Area="165 120 250 146"/>
		<Key IsCtrlKey="0" ASCIIKey="." IsInputMethod="0"/>
		<Label Status="0" Bold="1" LaIndexID="........" CharSize="8 168 168 168 168 168 168 1614" LaFrnColor="0xffffff -1"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_7">
		<General Desc="KY_0" FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="8 3" Area="165 92 250 120"/>
		<Key IsCtrlKey="0" ASCIIKey="3" IsInputMethod="0"/>
		<Label Status="0" Bold="0" LaIndexID="33333333" CharSize="8 168 168 168 168 168 168 1614" LaFrnColor="0xffffff -1"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_8">
		<General Desc="KY_0" FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="2 17" Area="248 92 358 146"/>
		<Key IsCtrlKey="1" ASCIIKey="0" IsInputMethod="0"/>
		<Label Status="0" Bold="0" LaIndexID="EntEntEntEntEntEntEntEnt" CharSize="8 168 168 168 168 168 168 1614" LaFrnColor="0xffff99 -1"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_12">
		<General Desc="KY_0" FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="10 12" Area="165 146 359 174"/>
		<Key IsCtrlKey="1" CtrlKey="3" ASCIIKey="0" IsInputMethod="0"/>
		<Label Status="0" Bold="0" LaIndexID="ExitExitExitExitExitExitExitExit" CharSize="8 168 168 168 168 168 168 1614" LaFrnColor="0xffffff -1"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_13">
		<General Desc="KY_0" FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="2 10" Area="248 66 358 92"/>
		<Key IsCtrlKey="1" CtrlKey="2" ASCIIKey="0" IsInputMethod="0"/>
		<Label Status="0" Bold="0" LaIndexID="ClrClrClrClrClrClrClrClr" CharSize="8 168 168 168 168 168 168 1614" LaFrnColor="0xffffff -1"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_14">
		<General Desc="KY_0" FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="2 10" Area="248 39 358 66"/>
		<Key IsCtrlKey="1" CtrlKey="1" ASCIIKey="0" IsInputMethod="0"/>
		<Label Status="0" Bold="0" LaIndexID="DelDelDelDelDelDelDelDel" CharSize="8 168 168 168 168 168 168 1614" LaFrnColor="0xffffff -1"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_15">
		<General Desc="KY_0" FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="10 12" Area="0 146 167 174"/>
		<Key IsCtrlKey="0" ASCIIKey="-" IsInputMethod="0"/>
		<Label Status="0" Bold="1" LaIndexID="+/-+/-+/-+/-+/-+/-+/-+/-" CharSize="8 168 168 168 168 168 168 1614" LaFrnColor="0xffffff -1"/></PartInfo>
	<PartInfo PartType="String" PartName="STR_0">
		<General Desc="STR_0" WordAddr="HSW00045" Fast="0" stCount="15" IsInput="0" WriteAddr="HSW045" KbdScreen="-1" IsPopKeyBrod="0" FigureFile="" BorderColor="0x101010 16777215" FrnColor="0x101010 -1" BgColor="0xcccccc -1" CharSize="8 16" IsHideNum="0" Transparent="0" IsShowPwd="0" IsIndirectR="0" IsIndirectW="0" IsInputDefault="0" IsDWord="0" IsHiLowRever="0" Area="1 1 358 22"/>
		<Extension IsCheck="0" Lockmate="0" DrawLock="0" LockMode="0" UseShowHide="0" HideType="0" IsHideAllTime="0" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
		<MoveZoom DataFormatMZ="2"/></PartInfo>
	<PartInfo PartType="Text" PartName="TXT_0">
		<General TextContent="MinMinMinMinMinMinMinMin" LaFrnColor="0x101010 -1" IsBackColor="0" BgColor="0x0 0" CharSize="8 168 168 168 168 168 168 1614" Bold="0" StartPt="21 22"/>
		<MoveZoom DataFormatMZ="2"/></PartInfo>
	<PartInfo PartType="Text" PartName="TXT_1">
		<General TextContent="MaxMaxMaxMaxMaxMaxMaxMax" LaFrnColor="0x101010 -1" IsBackColor="0" BgColor="0x0 0" CharSize="8 168 168 168 168 168 168 1614" Bold="0" StartPt="198 21"/>
		<MoveZoom DataFormatMZ="2"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_16">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="297 174 358 201"/>
		<Key IsCtrlKey="0" ASCIIKey="F" IsInputMethod="0"/>
		<Label Status="0" Bold="0" LaIndexID="FFFFFFFF" CharSize="8 168 168 168 168 168 168 1614" LaFrnColor="0xffffff -1"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_17">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="238 174 299 201"/>
		<Key IsCtrlKey="0" ASCIIKey="E" IsInputMethod="0"/>
		<Label Status="0" Bold="0" LaIndexID="EEEEEEEE" CharSize="8 168 168 168 168 168 168 1614" LaFrnColor="0xffffff -1"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_18">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" LaStartPt="18 10" Area="178 174 240 201"/>
		<Key IsCtrlKey="0" ASCIIKey="D" IsInputMethod="0"/>
		<Label Status="0" Bold="0" LaIndexID="DDDDDDDD" CharSize="8 168 168 168 168 168 168 1614" LaFrnColor="0xffffff -1"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_19">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="119 174 180 201"/>
		<Key IsCtrlKey="0" ASCIIKey="C" IsInputMethod="0"/>
		<Label Status="0" Bold="0" LaIndexID="CCCCCCCC" CharSize="8 168 168 168 168 168 168 1614" LaFrnColor="0xffffff -1"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_20">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="60 174 121 201"/>
		<Key IsCtrlKey="0" ASCIIKey="B" IsInputMethod="0"/>
		<Label Status="0" Bold="0" LaIndexID="BBBBBBBB" CharSize="8 168 168 168 168 168 168 1614" LaFrnColor="0xffffff -1"/></PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_21">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="44 11" Area="0 174 61 201"/>
		<Key IsCtrlKey="0" ASCIIKey="A" IsInputMethod="0"/>
		<Label Status="0" Bold="0" LaIndexID="AAAAAAAA" CharSize="8 168 168 168 168 168 168 1614" LaFrnColor="0xffffff -1"/></PartInfo></ScrInfo>
