<?xml version="1.0" encoding="UTF-8"?>
<ScrInfo ScreenNo="4" ScreenType="" ScreenSize="0">
	<Script>
		<TrigAction>
		</TrigAction>
		<TimerAction>
			<Timer Interval="5">@W_D24 = @W_HDW8050
</Timer></TimerAction></Script>
	<PartInfo PartType="Rect" PartName="REC_3">
		<General Area="168 172 473 216" BorderColor="0x0 0" Pattern="1" FrnColor="0xffffff -1" BgColor="0xffffff 0" ActiveColor="0"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="Rect" PartName="REC_2">
		<General Area="7 120 473 162" BorderColor="0x0 0" Pattern="1" FrnColor="0xffffff 0" BgColor="0xffffff 0" ActiveColor="0"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="Rect" PartName="REC_1">
		<General Area="7 44 473 79" BorderColor="0x0 0" Pattern="1" FrnColor="0xffffff 0" BgColor="0xffffff 0" ActiveColor="0"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="BitSwitch" PartName="BS_0">
		<General Desc="BS_0" Area="362 48 434 74" OperateAddr="M22" Fast="0" BitFunc="2" ResetDelay="100" Monitor="1" MonitorAddr="M22" FigureFile="TFT-type style\TFT001.pvg" BorderColor="0xcccccc 0" BmpIndex="-1" Align="3" LaStartPt="19 7" BitShowReverse="0" UseGlint="0" UseShowHide="0" HideType="0" IsHideAllTime="0" IsIndirectR="0" IsIndirectW="0"/>
		<Extension IsCheck="0" Lockmate="0" DrawLock="0" IsShowGrayScale="0" LockMode="0" TouchState="1" Buzzer="1" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/>
		<Label Status="0" Pattern="1" FrnColor="0xffffff 1" BgColor="0xffffff 0" Bold="0" LaIndexID="Старт" CharSize="1717171717171717" LaFrnColor="0x0 0"/>
		<Label Status="1" Pattern="1" FrnColor="0xffffff 0" BgColor="0xffffff 0" Bold="0" LaIndexID="Старт" CharSize="1717171717171717" LaFrnColor="0x0 0"/></PartInfo>
	<PartInfo PartType="Numeric" PartName="NUM_0">
		<General Desc="NUM_0" Area="337 85 460 116" WordAddr="HDW8050" Fast="0" IsInput="1" WriteAddr="HDW8050" KbdScreen="1000" IsPopKeyBrod="0" FigureFile="TFT-type style\dp_zc00.pvg" BorderColor="0xcccccc 0" FrnColor="0x0 0" BgColor="0xffffff 0" BmpIndex="-1" Transparent="0" IsHideNum="0" HighZeroPad="0" IsShowPwd="0" UseGlint="0" GlintFgClr="0x0 0" ZeroNoDisplay="0" IsIndirectR="0" IsIndirectW="0" IsAddFrame="0" IsWordOrder="0"/>
		<DispFormat DispType="2" DigitCount="5 0" DataLimit="0 1199570688" DataRange="0.000000 65535.000000" IsVar="0" Zoom="0" Mutiple="1.000000" Round="0" CharSize="17" IsInputLabelL="0" IsInputLabelR="0" IsInputDefault="0" bShowRange="0" IsVar1="0" ColorHText="0x0 0" ColorHBag="0x0 0" ColorLText="0x0 0" ColorLBag="0x0 0"/>
		<Extension IsCheck="0" Lockmate="0" DrawLock="0" LockMode="0" UseShowHide="0" HideType="0" IsHideAllTime="0" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="BitSwitch" PartName="BS_1">
		<General Desc="BS_0" Area="350 130 447 156" OperateAddr="M23" Fast="0" BitFunc="2" ResetDelay="500" Monitor="1" MonitorAddr="M23" FigureFile="TFT-type style\TFT001.pvg" BorderColor="0xcccccc 0" BmpIndex="-1" Align="3" LaStartPt="17 5" BitShowReverse="0" UseGlint="0" UseShowHide="0" HideType="0" IsHideAllTime="0" IsIndirectR="0" IsIndirectW="0"/>
		<Extension IsCheck="0" Lockmate="0" DrawLock="0" IsShowGrayScale="0" LockMode="0" TouchState="1" Buzzer="1" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/>
		<Label Status="0" Pattern="1" FrnColor="0xffffff 1" BgColor="0xffffff 0" Bold="0" LaIndexID="Применить" CharSize="1717171717171717" LaFrnColor="0x0 0"/>
		<Label Status="1" Pattern="1" FrnColor="0xffffff 0" BgColor="0xffffff 0" Bold="0" LaIndexID="Применить" CharSize="1717171717171717" LaFrnColor="0x0 0"/></PartInfo>
	<PartInfo PartType="Numeric" PartName="NUM_3">
		<General Desc="NUM_0" Area="337 179 460 210" WordAddr="D0" Fast="0" IsInput="0" WriteAddr="D0" KbdScreen="1000" IsPopKeyBrod="0" FigureFile="TFT-type style\dp_zc00.pvg" BorderColor="0xcccccc 0" FrnColor="0x0 0" BgColor="0xffffff 0" BmpIndex="-1" Transparent="0" IsHideNum="0" HighZeroPad="0" IsShowPwd="0" UseGlint="0" GlintFgClr="0x0 0" ZeroNoDisplay="0" IsIndirectR="0" IsIndirectW="0" IsAddFrame="0" IsWordOrder="0"/>
		<DispFormat DispType="6" DigitCount="5 0" DataLimit="3296313344 1191181824" DataRange="-999.000000 32767.000000" IsVar="0" Zoom="0" Mutiple="1.000000" Round="0" CharSize="17" IsInputLabelL="0" IsInputLabelR="0" IsInputDefault="0" bShowRange="0" IsVar1="0" ColorHText="0x0 0" ColorHBag="0x0 0" ColorLText="0x0 0" ColorLBag="0x0 0"/>
		<Extension IsCheck="0" Lockmate="0" DrawLock="0" LockMode="0" UseShowHide="0" HideType="0" IsHideAllTime="0" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="FunctionSwitch" PartName="FS_0">
		<General Desc="FS_0" Area="10 224 46 251" ScrSwitch="1" ScreenNo="10" ScreenNo2="-1" PointPos="0 0" PopupScreenType="0" PopupCloseWithParent="0" FigureFile="Arrow-type style\swjt_a03.pvg" BorderColor="0xcccccc 0" Pattern="7471169" FrnColor="0x0 0" BgColor="0x0 0" BmpIndex="-1" LaStartPt="18 13" UseShowHide="0" HideType="0" IsHideAllTime="0"/>
		<Extension Lockmate="0" DrawLock="0" IsShowGrayScale="0" LockMode="0" TouchState="1" Buzzer="1" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/>
		<Label Status="0" Bold="0" CharSize="1414141414141414" LaFrnColor="0x0 0"/></PartInfo>
	<PartInfo PartType="BitSwitch" PartName="BS_4">
		<General Desc="BS_0" Area="309 224 381 251" OperateAddr="M24" Fast="0" BitFunc="2" ResetDelay="100" Monitor="1" MonitorAddr="M24" FigureFile="TFT-type style\TFT001.pvg" BorderColor="0xcccccc 0" BmpIndex="-1" Align="3" LaStartPt="19 6" BitShowReverse="0" UseGlint="0" UseShowHide="0" HideType="0" IsHideAllTime="0" IsIndirectR="0" IsIndirectW="0"/>
		<Extension IsCheck="0" Lockmate="0" DrawLock="0" IsShowGrayScale="0" LockMode="0" TouchState="1" Buzzer="1" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/>
		<Label Status="0" Pattern="1" FrnColor="0xffffff 1" BgColor="0xffffff 0" Bold="0" LaIndexID="Сброс" CharSize="1717171717171717" LaFrnColor="0x0 0"/>
		<Label Status="1" Pattern="1" FrnColor="0xffffff 0" BgColor="0xffffff 0" Bold="0" LaIndexID="Сброс" CharSize="1717171717171717" LaFrnColor="0x0 0"/></PartInfo>
	<PartInfo PartType="FunctionSwitch" PartName="FS_1">
		<General Desc="FS_0" Area="421 224 457 251" ScrSwitch="1" ScreenNo="5" ScreenNo2="-1" PointPos="0 0" PopupScreenType="0" PopupCloseWithParent="0" FigureFile="Arrow-type style\swjt_a04.pvg" BorderColor="0xcccccc 0" Pattern="7471169" FrnColor="0x0 0" BgColor="0x0 0" BmpIndex="-1" LaStartPt="30 24" UseShowHide="0" HideType="0" IsHideAllTime="0"/>
		<Extension Lockmate="0" DrawLock="0" IsShowGrayScale="0" LockMode="0" TouchState="1" Buzzer="1" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/>
		<Label Status="0" Bold="0" CharSize="1414141414141414" LaFrnColor="0x0 0"/></PartInfo>
	<PartInfo PartType="Text" PartName="TXT_1">
		<General TextContent="1. Нажимаем СТАРТ" LaFrnColor="0x0 0" IsBackColor="0" BgColor="0xffffff 0" CharSize="176 126 126 126 126 126 126 12" Bold="0" StartPt="13 53"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="Rect" PartName="REC_0">
		<General Area="70 6 349 35" BorderColor="0x0 0" Pattern="1" FrnColor="0x80ff80 -1" BgColor="0x80ff -1" ActiveColor="0"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="Text" PartName="TXT_3">
		<General TextContent="КАЛИБРОВКА ТЕНЗОДАТЧИКА" LaFrnColor="0x0 0" IsBackColor="0" BgColor="0xffffff 0" CharSize="166 126 126 126 126 126 126 12" Bold="0" StartPt="79 11"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="Text" PartName="TXT_0">
		<General TextContent="2. Кладем калибровочный вес на весы
и вводим его значение в окошко, грамм" LaFrnColor="0x0 0" IsBackColor="0" BgColor="0xffffff 0" CharSize="176 126 126 126 126 126 126 12" Bold="0" StartPt="13 82"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="Text" PartName="TXT_2">
		<General TextContent="3. Нажимаем ПРИМЕНИТЬ. Реальный вес 
должен совпасть в весом калибровки" LaFrnColor="0x0 0" IsBackColor="0" BgColor="0xffffff 0" CharSize="176 126 126 126 126 126 126 12" Bold="0" StartPt="13 125"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="Text" PartName="TXT_4">
		<General TextContent="Реальный вес, грамм" LaFrnColor="0x0 0" IsBackColor="0" BgColor="0xffffff 0" CharSize="176 126 126 126 126 126 126 12" Bold="0" StartPt="181 184"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="Text" PartName="TXT_5">
		<General TextContent="Сброс параметров 
тензодатчика" LaFrnColor="0x0 0" IsBackColor="0" BgColor="0xffffff 0" CharSize="176 126 126 126 126 126 126 12" Bold="0" StartPt="148 219"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo></ScrInfo>
