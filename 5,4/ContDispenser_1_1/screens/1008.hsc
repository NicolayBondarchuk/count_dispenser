<?xml version="1.0" encoding="utf-8"?>
<ScrInfo ScreenNo="0" ScreenType="" ScreenSize="0">
	<PartInfo PartType="String" PartName="STR_0">
		<General Desc="STR_0" WordAddr="HSW45" stCount="15" IsInput="1" WriteAddr="HSW45" KbdScreen="-1" BorderColor="0xc0 16777215" FrnColor="0x0 -1" BgColor="0xcccccc -1" CharSize="16 32" IsShowPwd="1" Area="6 1 412 25"/>
		<Extension/>
		<MoveZoom DataFormatMZ="2"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_0">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="7 8" Area="0 135 41 169"/>
		<Key ASCIIKey=" "/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="SPASPASPASPASPASPASPASPA" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="BitSwitch" PartName="BS_0">
		<General Desc="BS_0" OperateAddr="HSX216.0" BitFunc="3" Monitor="1" MonitorAddr="HSX216.0" FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="7 9" Area="0 101 41 135"/>
		<Extension AckTime="20" TouchState="1" Buzzer="1"/>
		<MoveZoom DataFormatMZ="2"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="CAPCAPCAPCAPCAPCAPCAPCAP" CharSize="8 168 168 168 168 168 168 168 16"/>
		<Label Status="1" LaFrnColor="0xffff99 -1" LaIndexID="CAPCAPCAPCAPCAPCAPCAPCAP" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_1">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="6 31" Area="0 30 41 102"/>
		<Key IsCtrlKey="1" CtrlKey="3" ASCIIKey="A"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="ESCESCESCESCESCESCESCESC" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_2">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="382 31 419 65"/>
		<Key ASCIIKey="0"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="00000000" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_3">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="382 66 419 100"/>
		<Key ASCIIKey="P"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="PPPPPPPP" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_4">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="382 101 419 135"/>
		<Key IsCtrlKey="1" CtrlKey="1" ASCIIKey="A"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="DELDELDELDELDELDELDELDEL" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_5">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="10 10" Area="344 135 419 169"/>
		<Key IsCtrlKey="1" ASCIIKey="A"/>
		<Label Status="0" LaFrnColor="0xffff99 -1" LaIndexID="ENTENTENTENTENTENTENTENT" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_6">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="42 101 79 135"/>
		<Key ASCIIKey="A"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="AAAAAAAA" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_7">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="193 135 230 169"/>
		<Key ASCIIKey="B"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="BBBBBBBB" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_8">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="117 135 154 169"/>
		<Key ASCIIKey="C"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="CCCCCCCC" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_9">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="117 101 154 135"/>
		<Key ASCIIKey="D"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="DDDDDDDD" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_10">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="117 66 154 100"/>
		<Key ASCIIKey="E"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="EEEEEEEE" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_11">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="155 101 192 135"/>
		<Key ASCIIKey="F"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="FFFFFFFF" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_12">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="231 101 268 135"/>
		<Key ASCIIKey="H"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="HHHHHHHH" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_13">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="306 66 343 100"/>
		<Key ASCIIKey="I"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="IIIIIIII" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_14">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="268 101 306 135"/>
		<Key ASCIIKey="J"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="JJJJJJJJ" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_15">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="306 101 343 135"/>
		<Key ASCIIKey="K"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="KKKKKKKK" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_16">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="344 101 381 135"/>
		<Key ASCIIKey="L"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="LLLLLLLL" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_17">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="231 135 268 169"/>
		<Key ASCIIKey="N"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="NNNNNNNN" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_18">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="344 66 381 100"/>
		<Key ASCIIKey="O"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="OOOOOOOO" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_19">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="42 66 79 100"/>
		<Key ASCIIKey="Q"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="QQQQQQQQ" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_20">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="155 66 192 100"/>
		<Key ASCIIKey="R"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="RRRRRRRR" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_21">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="193 66 230 100"/>
		<Key ASCIIKey="T"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="TTTTTTTT" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_22">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="268 66 306 100"/>
		<Key ASCIIKey="U"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="UUUUUUUU" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_23">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="306 135 343 169"/>
		<Key IsCtrlKey="1" CtrlKey="2" ASCIIKey="A"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="CLRCLRCLRCLRCLRCLRCLRCLR" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_24">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="155 135 192 169"/>
		<Key ASCIIKey="V"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="VVVVVVVV" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_25">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="79 66 117 100"/>
		<Key ASCIIKey="W"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="WWWWWWWW" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_26">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="79 135 117 169"/>
		<Key ASCIIKey="X"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="XXXXXXXX" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_27">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="42 135 79 169"/>
		<Key ASCIIKey="Z"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="ZZZZZZZZ" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_28">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="193 31 230 65"/>
		<Key ASCIIKey="5"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="55555555" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_29">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="117 31 154 65"/>
		<Key ASCIIKey="3"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="33333333" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_30">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="79 31 117 65"/>
		<Key ASCIIKey="2"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="22222222" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_31">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="344 31 381 65"/>
		<Key ASCIIKey="9"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="99999999" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_32">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="306 31 343 65"/>
		<Key ASCIIKey="8"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="88888888" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_33">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="268 31 306 65"/>
		<Key ASCIIKey="7"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="77777777" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_34">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="231 31 268 65"/>
		<Key ASCIIKey="6"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="66666666" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_35">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="193 101 230 135"/>
		<Key ASCIIKey="G"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="GGGGGGGG" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_36">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="268 135 306 169"/>
		<Key ASCIIKey="M"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="MMMMMMMM" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_37">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="79 101 117 135"/>
		<Key ASCIIKey="S"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="SSSSSSSS" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_38">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="231 66 268 100"/>
		<Key ASCIIKey="Y"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="YYYYYYYY" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_39">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="13 5" Area="155 31 192 65"/>
		<Key ASCIIKey="4"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="44444444" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
	<PartInfo PartType="keystoke" PartName="KY_40">
		<General FigureFile="TFT-type style\TFT001.pvg" BorderColor="0x101010 -1" BmpIndex="-1" LaStartPt="18 10" Area="42 31 79 65"/>
		<Key ASCIIKey="1"/>
		<Label Status="0" LaFrnColor="0xffffff -1" LaIndexID="11111111" CharSize="8 168 168 168 168 168 168 168 16"/>
	</PartInfo>
</ScrInfo>
