<?xml version="1.0" encoding="utf-8"?>
<ScrInfo ScreenNo="0" ScreenType="" ScreenSize="0">
	<PartInfo PartType="Numeric" PartName="NUM_0">
		<General Desc="NUM_0" WordAddr="HSW28" IsInput="1" WriteAddr="HSW28" KbdScreen="-1" FigureFile="真彩型样式\dp_zc00.pvg" BorderColor="0xcccccc 0" BgColor="0xc08000 -1" BmpIndex="-1" Area="45 16 102 35"/>
		<DispFormat DispType="2" DigitCount="5 0" DataLimit="0 1199570688" Mutiple="1.000000" CharSize="14"/>
		<Extension AckTime="20"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="Numeric" PartName="NUM_1">
		<General Desc="NUM_0" WordAddr="HSW29" IsInput="1" WriteAddr="HSW29" KbdScreen="-1" FigureFile="真彩型样式\dp_zc00.pvg" BorderColor="0xcccccc 0" BgColor="0xc08000 -1" BmpIndex="-1" Area="45 42 102 61"/>
		<DispFormat DispType="2" DigitCount="2 0" DataLimit="0 1094713344" Mutiple="1.000000" CharSize="14"/>
		<Extension AckTime="20"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="Numeric" PartName="NUM_2">
		<General Desc="NUM_0" WordAddr="HSW30" IsInput="1" WriteAddr="HSW30" KbdScreen="-1" FigureFile="真彩型样式\dp_zc00.pvg" BorderColor="0xcccccc 0" BgColor="0xc08000 -1" BmpIndex="-1" Area="45 66 102 86"/>
		<DispFormat DispType="2" DigitCount="2 0" DataLimit="0 1106771968" Mutiple="1.000000" CharSize="14"/>
		<Extension AckTime="20"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="Numeric" PartName="NUM_3">
		<General Desc="NUM_0" WordAddr="HSW31" IsInput="1" WriteAddr="HSW31" KbdScreen="-1" FigureFile="真彩型样式\dp_zc00.pvg" BorderColor="0xcccccc 0" BgColor="0xc08000 -1" BmpIndex="-1" Area="45 115 102 134"/>
		<DispFormat DispType="2" DigitCount="2 0" DataLimit="0 1103101952" Mutiple="1.000000" CharSize="14"/>
		<Extension AckTime="20"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="Numeric" PartName="NUM_4">
		<General Desc="NUM_0" WordAddr="HSW32" IsInput="1" WriteAddr="HSW32" KbdScreen="-1" FigureFile="真彩型样式\dp_zc00.pvg" BorderColor="0xcccccc 0" BgColor="0xc08000 -1" BmpIndex="-1" Area="45 141 102 160"/>
		<DispFormat DispType="2" DigitCount="2 0" DataLimit="0 1114636288" Mutiple="1.000000" CharSize="14"/>
		<Extension AckTime="20"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="Numeric" PartName="NUM_5">
		<General Desc="NUM_0" WordAddr="HSW33" IsInput="1" WriteAddr="HSW33" KbdScreen="-1" FigureFile="真彩型样式\dp_zc00.pvg" BorderColor="0xcccccc 0" BgColor="0xc08000 -1" BmpIndex="-1" Area="45 165 102 184"/>
		<DispFormat DispType="2" DigitCount="2 0" DataLimit="0 1114636288" Mutiple="1.000000" CharSize="14"/>
		<Extension AckTime="20"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="Numeric" PartName="NUM_6">
		<General Desc="NUM_0" WordAddr="HSW126" IsInput="1" WriteAddr="HSW126" KbdScreen="-1" FigureFile="真彩型样式\dp_zc00.pvg" BorderColor="0xcccccc 0" BgColor="0xc08000 -1" BmpIndex="-1" Area="45 90 102 109"/>
		<DispFormat DispType="2" DigitCount="1 0" DataLimit="0 1091567616" Mutiple="1.000000" CharSize="14"/>
		<Extension/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="Text" PartName="TXT_1">
		<General BgColor="0xffffff 0" TextContent="YearYearYearYearYearYearYearYear" CharSize="1414141414141414" StartPt="106 18"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="Text" PartName="TXT_2">
		<General BgColor="0xffffff 0" TextContent="DayDayDayDayDayDayDayDay" CharSize="1414141414141414" StartPt="106 69"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="Text" PartName="TXT_3">
		<General BgColor="0xffffff 0" TextContent="MonMonMonMonMonMonMonMon" CharSize="1414141414141414" StartPt="106 44"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="Text" PartName="TXT_4">
		<General BgColor="0xffffff 0" TextContent="W.DayW.DayW.DayW.DayW.DayW.DayW.DayW.Day" CharSize="1414141414141414" StartPt="100 93"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="Text" PartName="TXT_5">
		<General BgColor="0xffffff 0" TextContent="HourHourHourHourHourHourHourHour" CharSize="1414141414141414" StartPt="106 117"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="Text" PartName="TXT_6">
		<General BgColor="0xffffff 0" TextContent="MinMinMinMinMinMinMinMin" CharSize="1414141414141414" StartPt="106 143"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="Text" PartName="TXT_7">
		<General BgColor="0xffffff 0" TextContent="SecSecSecSecSecSecSecSec" CharSize="1414141414141414" StartPt="106 167"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="FunctionSwitch" PartName="FS_0">
		<General Desc="FS_0" FuncFunc="8" ScreenNo="-1" ScreenNo2="-1" FigureFile="真彩型样式\真彩010.pvg" BorderColor="0xff80 -1" Pattern="-894245163" BmpIndex="-1" LaStartPt="14 4" Area="96 192 146 211"/>
		<Extension TouchState="1" Buzzer="1"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/>
		<Label Status="0" LaIndexID="OKOKOKOKOKOKOKOK" CharSize="1414141414141414"/></PartInfo>
	<PartInfo PartType="WordSwitch" PartName="WS_0">
		<General Desc="WS_0" WordAddr="HSW0" WriteAddr="HSW0" DataFormat="2" Const="1" BorderColor="0xc6a37d 0" BmpIndex="-1" LaStartPt="42 15" Area="96 193 146 210"/>
		<Extension AckTime="20" TouchState="1" Buzzer="1"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/>
		<Label Status="0" Pattern="34816" FrnColor="0xece9e6 0" BgColor="0xece9e6 0" LaFrnColor="0xff0000 0" CharSize="1414141414141414"/></PartInfo></ScrInfo>
