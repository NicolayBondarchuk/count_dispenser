<?xml version="1.0" encoding="UTF-8"?>
<ScrInfo ScreenNo="2" ScreenType="" ScreenSize="0">
	<Script>
		<InitialAction>@W_D10=@W_HDW8020
@W_D16=@W_HDW8021
@W_D13=@W_HDW8022
@W_D14=@W_HDW8023
@W_D15=@W_HDW8024 + @W_HDW8025
@W_D17=@W_HDW8026
@W_D18=@W_HDW8027
</InitialAction>
		<TimerAction>
			<Timer Interval="1">' показания расхода
dim x, y, k, a, b as floating
x = D2F(x, @W_D0)
k = asFloating(@W_HDW8044)
b = asFloating(@W_HDW8046)

if k = 0 then
k = 1
endif

y = (x - b) / k

@W_HDW8060 = y

' количество
a = asFloating(@W_HDW8062)
a = a + y / 7200
@W_HDW8062 = a

' скорость лотка в %: D15 / 2000 * 100
@W_HDW24 = @W_D15 / 20
</Timer></TimerAction>
		<TrigAction>
			<Trigger Action="1" BitAddr="X0">' обнуление счетчика
'@W_HDW8062 = 0			

'PID regulator
@W_D30 = @W_HDW8020					'Ts, 0...32767 мс
@W_D36 = @W_HDW8021					'Вх фильтр, 0...1024
@W_D33 = @W_HDW8022					'Kp, 0...32767
@W_D34 = @W_HDW8023					'Ki, 0...32767
@W_D35 = @W_HDW8024					'Kd, 0...32767 
@W_D37 = @W_HDW8026					'Нижняя граница, 0
@W_D38 = @W_HDW8027					'Верхняя граница, 2000

' запуск работы
@B_M15 = 1

'уставка
dim k, b, x, y as floating
k = asFloating(@W_HDW8044)
b = asFloating(@W_HDW8046)
y = asFloating(@W_HDW8040)

x = k * y + b

F2D(@W_D10, x)
</Trigger></TrigAction></Script>
	<PartInfo PartType="Rect" PartName="REC_1">
		<General Area="28 48 202 220" BorderColor="0x0 0" Pattern="1" FrnColor="0xdbfff8 -1" BgColor="0xffffff 0" ActiveColor="0"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="Rect" PartName="REC_0">
		<General Area="8 9 235 38" BorderColor="0x0 0" Pattern="1" FrnColor="0x80ff80 -1" BgColor="0x80ff -1" ActiveColor="0"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="Text" PartName="TXT_0">
		<General TextContent="ПАРЦИАЛЬНЫЙ РЕЖИМ" LaFrnColor="0x0 0" IsBackColor="0" BgColor="0xffffff 0" CharSize="166 126 126 126 126 126 126 12" Bold="0" StartPt="14 13"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="Numeric" PartName="NUM_1">
		<General Desc="NUM_0" Area="53 75 157 104" WordAddr="HDW8041" Fast="0" IsInput="1" WriteAddr="HDW8041" KbdScreen="1000" IsPopKeyBrod="0" FigureFile="TFT-type style\dp_zc00.pvg" BorderColor="0xcccccc 0" FrnColor="0x0 0" BgColor="0xffffff 0" BmpIndex="-1" Transparent="0" IsHideNum="0" HighZeroPad="0" IsShowPwd="0" UseGlint="0" GlintFgClr="0x0 0" ZeroNoDisplay="0" IsIndirectR="0" IsIndirectW="0" IsAddFrame="0" IsWordOrder="0"/>
		<DispFormat DispType="2" DigitCount="2 3" DataLimit="0 1115886060" DataRange="0.000000 65.535000" IsVar="0" Zoom="0" Mutiple="1.000000" Round="0" CharSize="16" IsInputLabelL="0" IsInputLabelR="0" IsInputDefault="0" bShowRange="0" IsVar1="0" ColorHText="0x0 0" ColorHBag="0x0 0" ColorLText="0x0 0" ColorLBag="0x0 0"/>
		<Extension IsCheck="0" Lockmate="0" DrawLock="0" LockMode="0" UseShowHide="0" HideType="0" IsHideAllTime="0" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="Text" PartName="TXT_2">
		<General TextContent="ДОЗА, кг" LaFrnColor="0x0 0" IsBackColor="0" BgColor="0xffffff 0" CharSize="176 126 126 126 126 126 126 12" Bold="0" StartPt="94 55"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="Numeric" PartName="NUM_2">
		<General Desc="NUM_0" Area="54 129 158 158" WordAddr="HDW7" Fast="0" IsInput="0" WriteAddr="HDW7" KbdScreen="1000" IsPopKeyBrod="0" FigureFile="TFT-type style\dp_zc00.pvg" BorderColor="0xcccccc 0" FrnColor="0x0 0" BgColor="0xffffff 0" BmpIndex="-1" Transparent="0" IsHideNum="0" HighZeroPad="0" IsShowPwd="0" UseGlint="0" GlintFgClr="0x0 0" ZeroNoDisplay="0" IsIndirectR="0" IsIndirectW="0" IsAddFrame="0" IsWordOrder="0"/>
		<DispFormat DispType="2" DigitCount="4 0" DataLimit="0 1176255488" DataRange="0.000000 9999.000000" IsVar="0" Zoom="0" Mutiple="1.000000" Round="0" CharSize="16" IsInputLabelL="0" IsInputLabelR="0" IsInputDefault="0" bShowRange="0" IsVar1="0" ColorHText="0x0 0" ColorHBag="0x0 0" ColorLText="0x0 0" ColorLBag="0x0 0"/>
		<Extension IsCheck="0" Lockmate="0" DrawLock="0" LockMode="0" UseShowHide="0" HideType="0" IsHideAllTime="0" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="Text" PartName="TXT_3">
		<General TextContent="КОЛИЧЕСТВО ДОЗ" LaFrnColor="0x0 0" IsBackColor="0" BgColor="0xffffff 0" CharSize="176 126 126 126 126 126 126 12" Bold="0" StartPt="47 110"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="FunctionSwitch" PartName="FS_0">
		<General Desc="FS_0" Area="13 230 49 257" ScrSwitch="1" ScreenNo2="-1" PointPos="0 0" PopupScreenType="0" PopupCloseWithParent="0" FigureFile="Arrow-type style\swjt_a03.pvg" BorderColor="0xcccccc 0" Pattern="7471169" FrnColor="0x0 0" BgColor="0x0 0" BmpIndex="-1" LaStartPt="30 24" UseShowHide="0" HideType="0" IsHideAllTime="0"/>
		<Extension Lockmate="0" DrawLock="0" IsShowGrayScale="0" LockMode="0" TouchState="1" Buzzer="1" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/>
		<Label Status="0" Bold="0" CharSize="1414141414141414" LaFrnColor="0x0 0"/></PartInfo>
	<PartInfo PartType="Text" PartName="TXT_1">
		<General TextContent="СКОРОСТЬ" LaFrnColor="0x0 0" IsBackColor="0" BgColor="0xffffff 0" CharSize="176 126 126 126 126 126 126 12" Bold="0" StartPt="53 166"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="Numeric" PartName="NUM_0">
		<General Desc="NUM_0" Area="53 185 158 214" WordAddr="HDW24" Fast="0" nuCount="1" IsInput="0" WriteAddr="HDW24" KbdScreen="1000" IsPopKeyBrod="0" FigureFile="TFT-type style\dp_zc00.pvg" BorderColor="0xcccccc 0" FrnColor="0x0 0" BgColor="0xffffff 0" BmpIndex="-1" Transparent="0" IsHideNum="0" HighZeroPad="0" IsShowPwd="0" UseGlint="0" GlintFgClr="0x0 0" ZeroNoDisplay="0" IsIndirectR="0" IsIndirectW="0" IsAddFrame="0" IsWordOrder="0"/>
		<DispFormat DispType="2" DigitCount="3 0" DataLimit="0 1120403456" DataRange="0.000000 100.000000" IsVar="0" Zoom="0" Mutiple="1.000000" Round="0" CharSize="16" IsInputLabelL="0" IsInputLabelR="0" IsInputDefault="0" bShowRange="0" IsVar1="0" ColorHText="0x0 0" ColorHBag="0x0 0" ColorLText="0x0 0" ColorLBag="0x0 0"/>
		<Extension IsCheck="0" Lockmate="0" DrawLock="0" LockMode="0" UseShowHide="0" HideType="0" IsHideAllTime="0" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="FlowPart" PartName="FL0_0">
		<General Area="170 233 350 253" LineType="5" BorderColor="0x40 -1" BmpIndex="2" FlowFrnColor1="0x8080ff -1" FlowBacColor1="0xff -1" BmpIndex1="3" FlowFrnColor2="0xffff -1" FlowBacColor2="0x808000 -1" FlowNo="5" TriggAddr="M15" HasBorderColor="0" DispDirect="3" FlowSpeed="4" UseShowHide="0" HideType="0" IsHideAllTime="0"/></PartInfo>
	<PartInfo PartType="Numeric" PartName="NUM_3">
		<General Desc="NUM_0" Area="312 41 460 85" WordAddr="HDW8060" Fast="0" nuCount="1" IsInput="0" WriteAddr="HDW8060" KbdScreen="1000" IsPopKeyBrod="0" FigureFile="TFT-type style\dp_zc00.pvg" BorderColor="0xcccccc 0" FrnColor="0x0 0" BgColor="0xffffff 0" BmpIndex="-1" Transparent="0" IsHideNum="0" HighZeroPad="0" IsShowPwd="0" UseGlint="0" GlintFgClr="0x0 0" ZeroNoDisplay="0" IsIndirectR="0" IsIndirectW="0" IsAddFrame="0" IsWordOrder="0"/>
		<DispFormat DispType="5" DigitCount="4 3" DataLimit="0 1176256511" DataRange="0.000000 9999.999000" IsVar="0" Zoom="0" Mutiple="1.000000" Round="0" CharSize="18" IsInputLabelL="0" IsInputLabelR="0" IsInputDefault="0" bShowRange="0" IsVar1="0" ColorHText="0x0 0" ColorHBag="0x0 0" ColorLText="0x0 0" ColorLBag="0x0 0"/>
		<Extension IsCheck="0" Lockmate="0" DrawLock="0" LockMode="0" UseShowHide="0" HideType="0" IsHideAllTime="0" IsUesPartPassword="0" IsSetLowerLev="0" IsUseUserAuthority="0"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="Text" PartName="TXT_5">
		<General TextContent="Расход, кг/час" LaFrnColor="0x0 0" IsBackColor="0" BgColor="0xffffff 0" CharSize="196 126 126 126 126 126 126 12" Bold="0" StartPt="329 20"/>
		<MoveZoom DataFormatMZ="2" DataLimitMZ="0 1199570688" MutipleMZ="1.000000"/></PartInfo>
	<PartInfo PartType="BarGraph" PartName="BG_0">
		<General Desc="BG_0" Area="347 87 425 257" WordAddr="HDW8060" DataFormat="2" IsVar="0" DataLimit="0 1112014848" YMinAddr="HDW2" YMaxAddr="HDW8040" FigureFile="compact style\bgjy_c01.pvg" BorderColor="0xcccccc 0" MeBgColor="0x80ffff -1" ShowAlarm="0" UpColor="0x0 -1" LowColor="0x0 0" IsAlarmVar="1" IsDoubleColor="0" Transparent="0" ShowReverse="0"/>
		<Graphics Pattern="7274595" FrnColor="0xffffff -1" BgColor="0xff00 -1" ShowMark="1" MarkColor="0x0 -1" MarkRange="0 1143111680" ShowAxisScale="1" Scale="10" LongIntval="5" ScaleColor="0x0 0" Show3DFrame="0" MetersFontSize="5"/></PartInfo></ScrInfo>
